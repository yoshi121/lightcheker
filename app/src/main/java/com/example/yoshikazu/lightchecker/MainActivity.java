package com.example.yoshikazu.lightchecker;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

public class MainActivity extends Activity implements SensorEventListener {

    //メンバー変数
    private SensorManager mSensorManager;    //センサーマネージャー
    private Sensor mLight;                    //光センサー
    private Boolean flag = true;
    private ImageButton imageButton;
    private static final int REQUEST_CODE = 1;
    int kagenti = 30;
    int jougenti = 750;
    TextView mTextOutputLight;                //光センサーの値

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //idの結び付け
        mTextOutputLight = (TextView) findViewById(R.id.TextOutputLight);
        imageButton = (ImageButton) findViewById(R.id.imageButton);

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v == imageButton) {
                    Intent intent = new Intent(MainActivity.this, settei.class);
                    startActivityForResult(intent, REQUEST_CODE);
                }
            }
        });
        //SensorManagerオブジェクトを取得
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        //光センサーである[Sensor.TYPE_LIGHT]を指定
        mLight = mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
    }

    @Override
    public void onResume() {
        super.onResume();
        //registerListener()でSensorEventListenerを登録
        //データ取得の間隔を指定
        //光センサーの場合
        mSensorManager.registerListener(this, mLight, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void onPause() {
        super.onPause();
        //unregisterListener()で、SensorEventListenerの登録解除
        mSensorManager.unregisterListener(this);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        if (event.sensor.getType() == Sensor.TYPE_LIGHT) {                    //光センサーを取得した場合
            //SensorEventのvalues配列をセットする(光の場合は1つしかない)
            mTextOutputLight.setText(String.valueOf(event.values[0]));
            if (event.values[0] < kagenti && flag == true) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                flag = false;
                builder.setMessage("明かりをつけてください").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        flag = true;
                    }
                });
                builder.show();
            } else if (event.values[0] > jougenti && flag == true) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                flag = false;
                builder.setMessage("少し暗くしましょう").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        flag = true;
                    }
                });
                builder.show();
            } else {

            }
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            //SecondActivityから戻ってきた場合
            case (REQUEST_CODE):
                if (resultCode == RESULT_OK) {

                } else if (resultCode == RESULT_CANCELED) {
                    //キャンセルボタンを押して戻ってきたときの処理
                } else {
                    //その他
                }
                break;
            default:
                break;
        }
    }
}