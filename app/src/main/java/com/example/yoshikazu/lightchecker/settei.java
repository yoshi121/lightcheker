package com.example.yoshikazu.lightchecker;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class settei extends AppCompatActivity {
    private Button button;
    private TextView textView5;
    private TextView textView4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settei);


        textView5 =(TextView)findViewById(R.id.textView5);
        textView4 =(TextView)findViewById(R.id.textView4);
        button = (Button)findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v == button) {
                    Intent intent = new Intent();
                    intent.putExtra("textView5",textView5.getText().toString());
                    intent.putExtra("textView4", textView4.getText().toString());
                    setResult(RESULT_OK,intent);
                    finish();

                }
            }
        });
    }
}
